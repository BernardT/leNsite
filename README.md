# note
There is an English wiki [there](https://gitlab.com/BernardT/leNsite/wikis/home).

Le _wiki_, en anglais,  est [ici](https://gitlab.com/BernardT/leNsite/wikis/home).

# le _N_ Site

Encore un autre moteur de blog, écrit avec _NodeJS_? Et pas fini, en plus! Et bien oui, écrire un tel moteur est une excellente manière d'apprendre à se servir de l'outil en question. En fait, j'utilise _NodeJS_, mais aussi _Express_ ce qui simplifie pas mal le travail.

Donc, ce projet assez banal est en quelque sorte ma ligne d'apprentissage de _NodeJS_ et de tout ce qui l'accompagne. Je dois dire que je suis particulièrement séduit par l'outil et n'ai plus du tout envie d'en changer pour la conception de mes sites personnels.

Quelques remarques rapides mais à ne pas négliger :
 - le look est à gerber? je ne suis pas un Web Designer, et il est loin d'être figé,
 - le concept de mon blog est ultra-classique? _Forkez_ le projet et faites ce qui vous plaît! et souvenez-vous de moi dans les citations...
 - le contenu (le texte, je veux dire) même peu glamour, c'est moi qui l'ai écrit, ne l'oubliez pas non plus dans vos citations...

 ___Bernard Tatin___

## la philosophie de ce mini-CMS
Je veux pouvoir éditer toutes les pages avec l'éditeur de mon choix (Vim, Brackets, ...) ou les éditer à travers une interface Web adaptée. Les pages, qui ne sont que de simples fichiers textes, plus précisement des extraits de fichiers HTML, sont sauvegardées avec Git.

Dit d'une autre manière, la base de données, c'est tout simplement le système de fichiers sous-jacent, de préférence un Unix. On peut trouver ce choix curieux, mais c'est mon choix!

## ce que j'ai oublié
La _philosophie Unix_ ou la _philosophie NodeJS_, c'est ça que j'ai oublié! On retrouve tout ici : [Unix Philosophy and Node.js](http://t.co/D6eYiEt8Tl).

# où en suis-je maintenant?
Au niveau des fonctionnalités, j'ai obtenu ce que je veux - ou presque, il manque une interface Web d'administration. Mais je ne suis pas satisfait pour de multiples raisons.

Le code n'est pas génial. Lorsque je me suis lancé dans l'aventure, je ne maîtrisais pas du tout _NodeJS_ et je n'ai pas anticipé vraiment la suite. De plus, il y a des points de _JavaScript_ que je ne maîtrise pas suffisamment pour en profiter pleinement, ce qui alourdit certainement mon code.

Le moteur et les données sont dans une même arborescence! C'est pas bien du tout, ça! Je ne devrais avoir qu'un seul moteur pour plusieurs occurrences de données, _i.e._ les données (pages mais aussi styles...) doivent sortir de cette arborescence.

## et alors?
On va donc réviser et _JavaScript_ et _NodeJS_ pour avoir un moteur beaucoup plus souple. Entre autre, utiliser au mieux les entrées/sorties asynchrones de _NodeJS_, les prototypes de _JavaScript_.
