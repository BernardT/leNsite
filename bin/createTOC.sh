#!/bin/sh

script=$(basename $0)

dohelp() {
    cat << HELP
${script} [--help] : this screen
${script} --book|--blog dir : dir is the place where to create TOC.json
HELP
exit 0
}

[ $# -ne 2 ] && dohelp

style=$1
dir=$2
toc=${dir}/TOC.json

gettitle() {
    file=$1
    egrep "<h2>.*</h2>" $file | head -n 1 | sed 's:<h2>\(.*\)</h2>:\1:'
}

dofilesbook() {
    for f in $(ls -c $dir/*.html)
    do
        echo "{ \"file\": \"$(basename $f)\", \"title\": \"$(gettitle $f)\" },"
    done
}
dofilesblog() {
    for f in $(ls $dir/*.html | sort -r)
    do
        echo "{ \"file\": \"$(basename $f)\", \"title\": \"$(gettitle $f)\" },"
    done
}
dofiles () {
    case $style in
        --book)
            dofilesbook
            ;;
        --blog)
            dofilesblog
            ;;
    esac
    echo '{ "file": "", "title": ""}'
}

cat > $toc << TOC
{
    "title": "TITLE",
    "pages": [
        $(dofiles)
    ]
}
TOC
