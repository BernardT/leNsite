#!/bin/sh

scriptname=$(basename $0)
basedir=$(dirname $(dirname $0))

npmstart () {
	echo "$scriptname Start"
	cd $basedir
	npm start &
}

npmstop () {
	echo "$scriptname Stop"
	cd $basedir
	npm stop
}

npmrestart () {
	echo "$scriptname Restart"
	cd $basedir
	npm stop
    npm start &
}

npmreload () {
	echo "$scriptname reload"
	npm restart
}

npmhelp () {
	cat << HELP
$scriptname [restart|start|stop|help|reload]
HELP
	exit 0
}

[ $# -eq 0 ] && npmhelp

cd $basedir

case $1 in
	start)
		npmstart
		;;
	stop)
		npmstop
		;;
	reload)
		npmreload
		;;
	restart)
		npmrestart
		;;
	help)
		npmhelp
		;;
	*)
		npmhelp
		;;
esac
exit 0
