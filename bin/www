#!/usr/bin/env node

'use strict';

/**
 * Module dependencies.
 */

let app = require('../app');
let debug = require('debug')('lensite:server');
let http = require('http');
let wwwHelper = require('../bttools/www-helper.js');
let port = wwwHelper.port;
let onError = wwwHelper.onError;

app.set('port', port);

/**
 * Create HTTP server.
 */

let server = http.createServer(app);

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  let addr = server.address();
  let bind = typeof addr === 'string' ? 
      'pipe ' + addr : 
      'port ' + addr.port;
  debug('Listening on ' + bind);
}

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);