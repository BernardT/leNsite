/**
 * loadfile.js
 */
let fs = require('fs');

module.exports = function() {
	return {
		loadfile: function(filename) {
			fs.readFile(filename, function(err, data) {
				if (err) {
					global.content = 'Cannot read ' + filename;
				} else {
					global.content = data;
				}
				global.done = true;
			});
		}
	};
};

