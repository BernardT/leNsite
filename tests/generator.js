/*
 * generator.js
 * author : bernard
 * date   : 21/10/2015 07:38:14
 */

'use strict';

function runTest(testFunction, limit, testName) {
    console.log("running " + testName);
    try {
        testFunction(limit);
        console.log("end of  " + testName);
    } catch (e) {
        console.error(testName + " failed! (" + e + ")");
    }
}

function* count(N) {
    console.log('begin count');
    for (let x = 0; x < N; x++) {
        console.log('count: ' + x);
        yield x;
    }
    console.log('end count');
}
/**
 * tester simplement un générateur qui affiche les
 * N premiers entiers
 * @param {[[Type]]} N [[Description]]
 */
function test1(N) {
    for (let x of count(N)) {
        console.log(x);
    }
}

function test2(N) {
    for (let x of count(N)) {
        console.log(x.value);
    }
}

function main() {
    runTest(test1, 12, "test1 : générateur d'entier simple");
    runTest(test2, 12, "test2 : générateur d'entier simple");
}

main();
