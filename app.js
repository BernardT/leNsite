/**
 * app.js
 * généré par l'installateur
 * modifié par bTaT1
 */


"use strict";

// NOTE: c'est un peu le bordel, là-dedans, non?
// ENCOURS: faut ranger tout ça!

const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const indexEngine = require('./routes/index');
const bookEngine = require('./routes/book-engine');

/**
 * configuration variables from json files
 */
const reload = require('./environment/globalEnv').reload;
const globalEnv = require('./environment/globalEnv').getEnv;
const version = globalEnv().version;
const baseurl = globalEnv().baseurl;
const title = globalEnv().siteName;
const pages = globalEnv().pages;

/**
 * personnal functions
 */

const now = require('./bttools/date-tools').now;

const app = express();


function gentleStop() {
    console.log('Gentle exit !!!!!');
    process.exit(0);
}

process.title = 'leNsite';
process.on('SIGUSR1', reload);
process.on('SIGUSR2', gentleStop);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
// DONE: using Less! I don't like stylus!
// FUTURE : learn more about less!
// app.use(require('less').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

// DONE : ce routage n'est pas parfait: GET /manuel/ passe dans index.js pas book-engine.js!
// DONE : la route '/' doit etre la dernière à être configurée
for (let page of pages) {
    // NOTE the main type must be the last to be processed
    // NOTE see site-configuration.json, all "__comments" fields
    if (page.type === 'main') {
        app.use(page.url, indexEngine);
    } else {
        app.use(page.url, bookEngine);
    }
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err,
            title: title,
            pagetitle: 'ERROR',
            now: now(),
            content: '',
            sidemenu: '',
            baseurl: baseurl,
            version: version
        });
    });
}



// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// not tested!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// production error handler
// no stacktraces leaked to user
// TEST: test me
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {},
        title: title,
        pagetitle: 'ERROR',
        now: now(),
        content: '',
        sidemenu: '',
        baseurl: baseurl,
        version: version
    });
});

module.exports = app;
