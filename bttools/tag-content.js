/**
 * tag-content.js
 */


'use strict';

// TODO: refaire ce truc pas fiable
const getFirstTag = function getFirstTag(tag, text) {
    let result = '';
    const tagStart = '<' + tag + '>';
    const tagEnd = '</' + tag + '>';

    let start = text.indexOf(tagStart);

    if (start != -1) {
        start += tag.length + 2;
        const end = text.indexOf(tagEnd, start);

        if (end !== -1) {
            result = text.substring(start, end);
        }
    }
    return result;
};

/**
 * Gets the content of the first 'h2' tag found in 'text'
 * @param   {String} text text, an HTML file
 * @returns {String} The content of the first 'h2' tag.
 */
const getFirstH2 = function getFirstH2(text) {
    return getFirstTag('h2', text);
};

const tagContent = {
    getFirstTag: getFirstTag,
    getFirstH2: getFirstH2
};

module.exports = tagContent;
