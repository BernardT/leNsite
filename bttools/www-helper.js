/*
 * www-helper.js
 * author : bernard
 * date   : 19/10/2015 23:33:14
 */

'use strict';

// <added by bTaT1>
const sport = require('../environment/globalEnv').getEnv().port;
// </added by bTaT1>

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

 /**
 * Get port from environment and store in Express.
 */

const port = normalizePort(sport);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}


const wwwHelper = {
    port: port,
    onError: onError
};

module.exports = wwwHelper;
