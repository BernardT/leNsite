/**
 * date-tools.js
 */

'use strict';

const moment = require('moment');

// TODO: peut-être rajouter une fonction pour le jour et une autre pour l'heure
        // return current date as a string
const now = function now () {
    return moment().format('DD/MM/YYYY à HH:mm:ss');
};

const dateTools = {
    now: now
};

module.exports = dateTools;
