/**
 * jsonLoader.js
 * load JSON files
 */

"use strict";

const fs = require('fs');

// TODO: Dans certains cas, la sortie du programme est la pire des choses!
// DONE: faire quelque chose en cas d'erreur !!!
// DONE: c'est tellement important, qu'il faut sortir du programme avec un beau message d'erreur
function _loadJson(jsonFile) {
    const jsonData = fs.readFileSync(jsonFile);
    return JSON.parse(jsonData.toString());
}

let loadJson = function(jsonFile) {
    let data = null;
    try {
        // wanted to be sure that some files were loaded done once and only once ...
        // ... and it works!
        data = _loadJson(jsonFile);
    } catch (e) {
        data = null;
        console.error('ERROR : bad JSON format of ' + jsonFile + ' (-- ' + e + ' --)');
    }
    return data;
};

const jsonLoader = {
    loadJson: loadJson
};

module.exports = jsonLoader;
