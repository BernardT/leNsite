## NEWS
### le 22/10/2015, 07:40
La dernière version en cours, 0.3.0, contient des améliorations internes importantes peu visibles mais cruciales. Le futur? 
 - continuer à utiliser les paramètres des fichiers de configuration, 
 - créer un environnement local,
 - créer des outils de gestion des pages en ligne de commande mais écrits avec _NodeJS_.
 
### le 16/10/2015, 15:37
Le code est assez propre et satisfaisant même si je doit gérer encore beaucoup de la configuration manuellement. On va maintenant faire un relooking total de la mort, la présentation actuelle est vraiment crade!

### le 15/10/2015, 18:41
J'ai obtenu le CMS de la mort qui tue avec l'organisation décrite plus bas. Au final, j'ai quelque chose qui tourne bien, qui correspond en grande partie à ce que je voulais :
 - édition des textes avec l'éditeur de mon choix,
 - aide à la génération des menus.

Tout ça, bien évidemment, en ligne de commande! Avant l'interface Web de création des pages, on peut se faire un joli script shell.Les étapes suivantes sont donc :
 - nettoyer le code,
 - créer un script shell d'assistance à la création des pages,
 - créer une interface d'administration qui facilite la tâche de création des textes et des TOC.

Le dernier problème sera d'ajouter un blog ou un livre supplémentaire.

### le 15/10/2015, 14:00
J'ai bien avancé, maintenant, je dois réfléchir à ce dont j'ai vraiment besoin, ce qui n'est pas toujours le plus simple. En fait, j'ai simplement besoin d'une structure de _livre_, avec _chapitres_, _sections_ et _sous-sections_. Le blog en est un cas particulier. Pour en arriver là, chaque _livre_ doit contenir sa table des matières avec sa structure représentée au format _JSON_. Le stockage des _articles_ est fait sous forme de fichiers placés dans une structure de répertoires représantant celle du _livre_.

Le challenge technique semble être le partage du code entre plusieurs URL. Mais ça se résoud bien...
