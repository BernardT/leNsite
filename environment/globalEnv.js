/*
 * globalEnv.js
 */

'use strict';

let exit = require('process').exit;
let loadJson = require('../bttools/jsonLoader').loadJson;

let env = null;

let TheGlobalEnv = function() {
    // package.json
    // name = "Site Name";
    this.version = "site-version";
    ////  apptitle = "App Title";
    ////  ////  description = "App description";
    this.author = "App author(s)";
    // site-configuration.json
    this.baseurl = "/baseURL/fromNGINX";
    this.port = 3001;
    this.siteName = "the name of the site";
    this.baseContentDir = "public/the-content";
    this.pages = null;
};

function getJSONdata(jsonFile) {
    let data = loadJson(jsonFile);
    if (!data) {
        // NOTE: peut-on avoir des valeurs par défaut?
        exit(1);
    }
    if (!data) {
        console.error('ERROR: can\'t read ' + jsonFile + ' !!!!');
        exit(1);
    }
    return data;
}

let getEnv = function () {
    if (!env) {
        // DONE: comprendre pourquoi on passe plusieurs fois ici!
        // avec 'use strict' et pas 'uses strict',
        // ensuite pas de this
        let data = null;

        env = new TheGlobalEnv();
        data = getJSONdata('package.json');
        env.version = data.version;
        env.author = data.author;

        data = getJSONdata('site-configuration.json');
        env.baseurl = data.baseurl;
        env.port = data.port;
        env.siteName = data.siteName;
        env.pages = data.pages;
        env.baseContentDir = data.baseContentDir;
    }
    return env;
};

let reload = function() {
    env = null;
    return getEnv();
};

let globalEnv = {
    env: null,
    getEnv: getEnv,
    reload: reload
};


module.exports = globalEnv;
