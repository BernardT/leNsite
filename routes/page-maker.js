/*
 * page-maker.js
 */


"use strict";

const EMPTY_MENU = "";
const EMPTY_SIDE_TEXT = "";
const EMPTY_PAGE = {
    fileName: null,
    index: null,
    title: 'ERROR',
    aside: null,
    menu: EMPTY_MENU,
    sidetext: EMPTY_SIDE_TEXT
};

const makeBadPage = function () {
    return EMPTY_PAGE;
};

const makePage = function (index, title, menu, filename) {
    return {
        fileName: filename,
        index: index,
        title: title,
        aside: null,
        menu: menu,
        sidetext: EMPTY_SIDE_TEXT
    };
};

const pageMaker = {
    empty_menu: EMPTY_MENU,
    empty_sidetext: EMPTY_SIDE_TEXT,
    makeBadPage: makeBadPage,
    makePage: makePage
};

module.exports = pageMaker;
