/*
 * maual.js
 */

// TODO: défaire ce bordel
// ENCOURS: défaire ce bordel
// TODO: virer le debug, mais pas tout de suite, ça peut-être encore utile!

'use strict';

const fs = require('fs');
const path = require('path');

const router = require('express').Router();
// const router = express.Router();

const baseContentDir = require('../environment/globalEnv').getEnv().baseContentDir;

const loader = require('./mw-fileloader');
const loadPage = loader.loadPage;
const getMenu = loader.getMenu;

function getPageName(page, req) {
    if (req.params.page === 'index') {
        return page.index;
    } else {
        return req.params.page;
    }
}

function _loadPage(req, res, next) {
    console.log('book::_loadPage: req.params.page -> ' + req.params.page);
    const baseFolder = baseContentDir + req.baseUrl;
    getMenu(baseFolder, req.baseUrl, function (page) {
        let pageName = getPageName(page, req);
        page.fileName = baseFolder + '/' + pageName + '.html';
        loadPage(page, req, res, next);
    });
}

router.get('/', function (req, res, next) {
    console.log('on GET /');
    req.params.page = 'index';
    _loadPage(req, res, next);
});

router.get('/:page', function (req, res, next) {
    console.log('on GET /' + req.params.page);
    _loadPage(req, res, next);
});

module.exports = router;
