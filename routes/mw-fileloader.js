/*
 * File:   mw-fileloader.js
 * Author: bernard
 *
 * Created on 2015-10-13 01:33
 * This is simple middleware to read and render pages
 */


"use strict";

const fs = require('fs');
const path = require('path');

/**
 * configuration variables
 */
const globalEnv = require('../environment/globalEnv').getEnv;
const version = globalEnv().version;
const baseurl = globalEnv().baseurl;
const siteName = globalEnv().siteName;
const title = globalEnv().siteName;

const now = require('../bttools/date-tools').now;
const getFirstH2 = require('../bttools/tag-content').getFirstH2;

const pagemaker = require('./page-maker');
const makePage = pagemaker.makePage;
const makeBadPage = pagemaker.makeBadPage;
// TODO: faire un fichier/module de constantes
// TODO: envisager la traduction
const JADE_TEMPLATE = 'blogmodele';
const ERROR_TITLE = 'E.R.R.E.U.R';
const PAGE_DONT_EXIST = '<p>La page demandée n\'existe pas</p>';

const menuTitle = "<h3>le menu</h3>\n";

function* tocParser(tocData) {
    for (let page of tocData.pages) {
        const f = path.basename(page.file, '.html');

        if (f !== '') {
            yield {
                pageTitle: page.title,
                f: f
            };
        }
    }
}

const niceReadFile = function (fileName, fOnData, fOnError, fOnNofile) {
    if (fileName === null || typeof fileName === undefined) {
        fOnNofile();
    } else {
        fs.readFile(fileName, function(err, data) {
            if (err) {
                fOnError(fileName, err);
            } else {
                try {
                    fOnData(data);
                } catch (e) {
                    fOnError(fileName, e);
                }
            }
        });
    }
};

const getMenu = function(baseFolder, reqBaseUrl, callback) {
    const toc = baseFolder + '/TOC.json';
    niceReadFile(toc,
        function onData(jsonTOC) {
            const data = JSON.parse(jsonTOC.toString());
            let menu = menuTitle;
            let index = null;
            const startHref = '<a href="' + baseurl + '/' + reqBaseUrl + '/';

            for (let token of tocParser(data)) {
                const endHref = '">' + token.pageTitle + "</a>\n";
                const f = token.f;

                if (!index) {
                    index = f;
                }
                menu += startHref + f + endHref;
            }
            let page = makePage(index, data.title, menu);
            page.aside = baseFolder + '/aside.html';
            console.log('page.aside 1 = ' + page.aside);
            callback(page);
        },
        function onError(fileName, err) {
            console.error("ERROR: fs.readfile of TOC failed -> " + err);
            let page = makeBadPage();
            page.aside = baseFolder + '/aside.html';
            console.log('page.aside 2 = ' + page.aside);
            callback(page);
        },
        function onNofile() {
            callback(makeBadPage());
        });
};

/**
 * Exported functions
 * @returns {Object} Exported functions
 */
/**
 * Load an HTML file and render it
 * @param {String} fileName name of the HTML file
 * @param {String} menu     menu content (side menu)
 * @param {Object} req      see router.get
 * @param {Object} res      see router.get
 * @param {Object} next     see router.get
 */
const loadPage = function loadPage(page, req, res, next) {
    let content = null;
    let pagetitle = null;
    let doRender = function() {
        res.render(JADE_TEMPLATE, {
            title: title,
            pagetitle: pagetitle,
            now: now(),
            content: content,
            sidemenu: page.menu,
            sidetext: page.sidetext,
            baseurl: baseurl,
            version: version
        });
    };
    let loadAside = function() {
        niceReadFile(page.aside,
            function onData(data) {
                page.sidetext = data;
                doRender();
            },
            function onError(fileName, err) {
                console.error('ERROR -> Cannot load ' + fileName + ' -> ' + err);
                doRender();
            },
            function onNoFile() {
                doRender();
            });
    };
    niceReadFile(page.fileName,
        function onData(data) {
            console.log("loadPage 2 (ok)");
            content = data.toString();
            pagetitle = getFirstH2(content);
            loadAside();
        },
        function onError(fileName, err) {
            console.log("loadPage (err 1)");
            pagetitle = ERROR_TITLE;
            content = PAGE_DONT_EXIST;
            loadAside();
        },
        function onNoFile() {
            console.log("loadPage (err 2)");
            pagetitle = ERROR_TITLE;
            content = PAGE_DONT_EXIST;
            loadAside();
        });
};

const fileLoader = {
    loadPage: loadPage,
    niceReadFile: niceReadFile,
    getMenu: getMenu
};

module.exports = fileLoader;
