/**
 * index.js
 * ce source n'existe que parce que je pars du principe que le traitement de
 * l'ensemble des textes de 'main' (contenant la page de départ, à propos...)
 * peut avoir un traitement *très* différent du reste comme par exemple,
 * ne jamais avoir de 'table des matières', mais est-ce une bonne idée?
 */

"use strict";

const fs = require('fs');
const express = require('express');
const router = express.Router();

const now = require('../bttools/date-tools').now;

const getGlobalEnv = require('../environment/globalEnv').getEnv;
const baseurl = getGlobalEnv().baseurl;
const baseContentDir = getGlobalEnv().baseContentDir;
const siteName = getGlobalEnv().siteName;
const pages = getGlobalEnv().pages;

const loadPage = require('./mw-fileloader').loadPage;
const getMenu = require('./mw-fileloader').getMenu;

const pagemaker = require('./page-maker');
const makePage = pagemaker.makePage;
const makeBadPage = pagemaker.makeBadPage;

const currentMenu = pagemaker.empty_menu;

let baseDir = null;

function getBasePageDir() {
    function getBaseDir() {
        for (let page of pages) {
            if (page.type === 'main') {
                baseDir = '/' + page.dir;
                break;
            }
        }
    }

    if (!baseDir) {
        getBaseDir();
    }
    return baseContentDir + baseDir;
}
const basePageDir = getBasePageDir();

function _loadPage(pageName, req, res, next) {
    console.log('index::_loadPage ' + pageName);
    console.log('index::_loadPage: req.params.page -> ' + req.params.page);
    let page = makePage('index', 'title', currentMenu, basePageDir + pageName + '.html');
    page.aside = basePageDir + 'aside.html';

    loadPage(page, req, res, next);
}

router.get('/', function (req, res, next) {
    _loadPage('index', req, res, next);
});

router.get('/:page', function (req, res, next) {
    _loadPage(req.params.page, req, res, next);

});


module.exports = router;
